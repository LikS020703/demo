package com.example.demo.controllers;

import java.net.PortUnreachableException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.models.Dog;

@RestController
public class ControllerDemo {
    private List<Dog> dogs = new ArrayList<Dog>();

    @GetMapping("/demo/list")
    public List<Dog> getList() {
        return dogs;
    }

    @PostMapping("/demo/add")
    public void addTeam(@RequestBody Dog dog) {
        dogs.add(dog);
    }
}
